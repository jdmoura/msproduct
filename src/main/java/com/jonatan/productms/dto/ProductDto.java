package com.jonatan.productms.dto;

import java.math.BigDecimal;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.jonatan.productms.model.Product;

public class ProductDto {
	
	@NotBlank
	private String name;
	@NotBlank
	private String description;
	@NotNull
	@DecimalMin(value = "0.0", inclusive = false)
	private BigDecimal price;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	
	public Product toProduct() {
		return new Product(name, description,price);
	}
	
}
