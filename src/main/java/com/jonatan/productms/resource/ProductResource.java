package com.jonatan.productms.resource;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jonatan.productms.dto.ProductDto;
import com.jonatan.productms.model.Product;
import com.jonatan.productms.service.ProductServiceImpl;


import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(path = "/products", produces ="application/json")
@CrossOrigin(origins = "*")
public class ProductResource {
	
	@Autowired
	ProductServiceImpl productService;
	
	@ApiOperation(value = "Criação de um produto")
	@ApiResponses(value = {
	    @ApiResponse(code = 201, message = "Retorna o produto criado."),
	    @ApiResponse(code = 400, message = "Requisição inválida")	   
	})	
	@PostMapping()
	public ResponseEntity<Product> createProduct(@Valid @RequestBody ProductDto product){		
		return productService.createNewProduct(product);	
	}
	
	@ApiOperation(value = "Busca todos os produtos cadastrados.")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Retorna os produtos cadastrados ou uma lista vazia")	  
	})
	@GetMapping()
	public ResponseEntity<List<Product>> getAllProduct(){
		return productService.findAllProduct();
	}
	
	@ApiOperation(value = "Busca um produto específico.")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Retorna o produto localizado."),
			@ApiResponse(code = 404, message = "Retorna 'not found' produto não lozalizado .")
	})
	@GetMapping(path = "/{id}")
	public ResponseEntity<Product> getProduct(@PathVariable Long id){
		return productService.getById(id);		
	}
	
	@ApiOperation(value = "Atualiza um produto específico.")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Produto Atualizado com sucesso."),
			@ApiResponse(code = 404, message = "Retorna 'not found' produto não lozalizado .")
	})
	@PutMapping(path = "/{id}")
	public ResponseEntity<Product> updateProduct(@PathVariable Long id, @Valid @RequestBody Product product) {
		return productService.updateById(product, id);		
	}
	
	@ApiOperation(value = "Busca os produtos de acordo com o filtro passado.")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Retorna os produtos filtrados ou uma lista vazia")			
	})
	@GetMapping(path = "/search")
	public ResponseEntity <List<Product>> getSuperSearch(@RequestParam(required = false, name = "q") String q, 
														 @RequestParam(required = false, name = "min_price") BigDecimal min_price,
														 @RequestParam(required =  false, name = "max_price")BigDecimal max_price){		
		return productService.filter(q, min_price, max_price);
		
	}	
	
	@ApiOperation(value = "Deleta um produto específico.")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Produto Deletado com sucesso."),
			@ApiResponse(code = 404, message = "Retorna 'not found' produto não lozalizado.")
	})
	@DeleteMapping(path = "/{id}")
	public ResponseEntity<?> deleteProduct(@PathVariable Long id){
		return productService.deleteProductById(id);
		
	}
}
