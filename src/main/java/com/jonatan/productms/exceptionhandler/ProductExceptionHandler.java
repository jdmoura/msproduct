package com.jonatan.productms.exceptionhandler;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ProductExceptionHandler extends ResponseEntityExceptionHandler {
	
	@Autowired
	private MessageSource messageSource;
	
	protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request){				
		String message = messageSource.getMessage("invalid.message", null, LocaleContextHolder.getLocale());
		
		return handleExceptionInternal(ex, new Error(HttpStatus.BAD_REQUEST.value(), message), 
				headers, HttpStatus.BAD_REQUEST, request);
	}
	
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		
		Error error = getAllError(ex.getBindingResult());
		
		return handleExceptionInternal(ex, error, headers, HttpStatus.BAD_REQUEST, request);
	}
	
	private Error getAllError(BindingResult bindingResult){		
		List <String> list =bindingResult.getFieldErrors()
				.stream()
				.map(FieldError ->{
					return messageSource
						.getMessage(FieldError, LocaleContextHolder.getLocale());
					
				}) 
				.collect(Collectors.toList());
		Error erro = new Error(HttpStatus.BAD_REQUEST.value(),list);
		
		
		return erro;
	}
	

}
