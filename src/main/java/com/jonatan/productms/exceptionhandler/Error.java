package com.jonatan.productms.exceptionhandler;

import java.util.List;

public class Error {
	
	private Integer status_code;	
	private String message;
	
	public Error(int status_code, List<String> message) {		
		this.status_code = status_code;
		this.message = message.toString();
	}
	
	public Error(int status_code, String message) {		
		this.status_code = status_code;
		this.message = message;
	}

	public Integer getStatus_code() {
		return status_code;
	}
	
	public String getMessage() {
		return message;
	}
	
	/*public String concatenateMessage(String message) {
		StringBuilder stringBuilderMessage = new StringBuilder(this.message);		
		stringBuilderMessage.append(message);
		stringBuilderMessage.append(" ; ");
		
		this.message = stringBuilderMessage.toString();
		
		return this.getMessage();
	}*/
	
}
