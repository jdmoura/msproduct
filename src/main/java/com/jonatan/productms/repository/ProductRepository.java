package com.jonatan.productms.repository;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.jonatan.productms.model.Product;
import com.jonatan.productms.repository.product.ProductRepositoryQuery;

public interface ProductRepository extends JpaRepository<Product, Long> , ProductRepositoryQuery, JpaSpecificationExecutor<Product> {
	
	List<Product> findByNameContaining(String name);
	List<Product> findByDescriptionContaining(String description);
	List<Product> findByPriceGreaterThanEqual(BigDecimal min_price);
	List<Product> findByPriceLessThanEqual(BigDecimal max_price);
	List<Product> findByNameContainingOrDescriptionContaining(String name, String description);
	List<Product> findByPriceBetween(BigDecimal min_price, BigDecimal max_price);
	List<Product> findByNameContainingOrDescriptionContainingAndPriceBetween(String name, String description, BigDecimal min_price, BigDecimal max_price);
	
	

}
