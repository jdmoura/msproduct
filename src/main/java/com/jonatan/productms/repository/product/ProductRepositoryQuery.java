package com.jonatan.productms.repository.product;

import java.util.List;

import com.jonatan.productms.model.Product;
import com.jonatan.productms.repository.filter.ProductFilter;

public interface ProductRepositoryQuery {
	
	 List<Product> filter(ProductFilter productFilter);
		
	

}
