package com.jonatan.productms.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.jonatan.productms.dto.ProductDto;
import com.jonatan.productms.model.Product;
import com.jonatan.productms.repository.ProductRepository;
import com.jonatan.productms.repository.filter.ProductFilter;


@Service
public class ProductServiceImpl implements ProductService {
	
	@Autowired
	ProductRepository productRepository;
	
	@Override
	public ResponseEntity<Product> createNewProduct(ProductDto productDto){
		Product productSaved = productRepository.save(productDto.toProduct());		
		return new ResponseEntity<>(productSaved, HttpStatus.CREATED);	
	}
	
	@Override
	public ResponseEntity<List<Product>> findAllProduct(){
		List<Product> listProducts = productRepository.findAll();
		return new ResponseEntity<>(listProducts, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Product> getById(Long id) {
		Optional<Product> product = productRepository.findById(id);	
		return product.map(prod -> new ResponseEntity<>(prod, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}
	
	@Override
	public ResponseEntity<Product> updateById(Product product ,Long id) {
		Optional<Product> productSaved = productRepository.findById(id);
		return productSaved.map(prod -> {
			BeanUtils.copyProperties(product, prod, "id");
			productRepository.save(productSaved.get());
			return new ResponseEntity<>(prod, HttpStatus.OK);
		}).orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}
	
	@Override
	public ResponseEntity<?> deleteProductById(Long id){
		Optional<Product> product = productRepository.findById(id);	
		   return product.map(prod -> {
			  productRepository.deleteById(id);
			   return ResponseEntity.ok().build();
		  }).orElse(ResponseEntity.notFound().build());
	}

	@Override
	public ResponseEntity<List<Product>> filter(String q, BigDecimal min_price, BigDecimal max_price ) {
		ProductFilter productFilter = new ProductFilter(q, min_price, max_price);
		List<Product> listProduct = productRepository.filter(productFilter);		
		
		return new ResponseEntity<>(listProduct, HttpStatus.OK); 
	}	

}
