package com.jonatan.productms.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.jonatan.productms.dto.ProductDto;
import com.jonatan.productms.model.Product;

public interface ProductService {
	
	ResponseEntity<Product> getById(Long id);
	
	ResponseEntity<Product> createNewProduct(ProductDto productDto);
	
	ResponseEntity<?> deleteProductById(Long id);
	
	ResponseEntity<Product> updateById(Product product ,Long id);
	
	ResponseEntity<List<Product>> findAllProduct();
	
	ResponseEntity<List<Product>> filter(String q, BigDecimal min_price, BigDecimal max_price);

}
